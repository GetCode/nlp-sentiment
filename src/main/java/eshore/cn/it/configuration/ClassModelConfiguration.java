package eshore.cn.it.configuration;
/**
 * 分类模型配置类
 * 配置模型分类的类别个数，配置模型训练样本的root位置
 * 目前支持的格式存放样本的格式有：
 * 1、在训练样本存放的root目录里面，按照类别名称，分别存放每个类别样本
 * @author clebeg
 * @time   2015-04-14 10:48
 */
public class ClassModelConfiguration {
	/**
	 * 类别数值，例如初始化为：{"军事", "经济", "政治"}
	 */
	private String[] categories;
	public String[] getCategories() {
		return categories;
	}
	public void setCategories(String[] categories) {
		this.categories = categories;
	}
	
	/**
	 * 训练样本的主目录，可以是相对路径，默认是data/training
	 * 如果有上面的类别，那么data/training目录中必须包含
	 * 军事/  经济/   政治/  这三个目录，分别存放对应类别的数据
	 */
	private String trainRootDir = "data/training";
	public String getTrainRootDir() {
		return trainRootDir;
	}
	public void setTrainRootDir(String trainRootDir) {
		this.trainRootDir = trainRootDir;
	}
	
	/**
	 * 训练好模型之后就需要保持模型，这就模型保持的目录及文件
	 */
	private String modelFile = "data/model/model.class";
	public String getModelFile() {
		return modelFile;
	}
	public void setModelFile(String modelFile) {
		this.modelFile = modelFile;
	}
	
	
}
